set number
set mouse=a
set numberwidth=1
set clipboard=unnamed
syntax enable
set showcmd
set ruler
set encoding=utf-8
set showmatch
set sw=2
set relativenumber

set laststatus=2
set noshowmode

" Cargar plugins
so ~/.config/nvim/plugins.vim

" Configuracion coc
so ~/.config/nvim/cocSettings.vim

" Atajos de teclado
so ~/.config/nvim/maps.vim

" Configuración plugins
so ~/.config/nvim/pluginsConfig.vim

colorscheme gruvbox 
let g:gruvbox_contrast_darl = "hard"
let NERDTreeQuitOnOpen=1
